<?php

namespace App\Http\Controllers;

use App\Models\Artikel;
use App\Models\Kategori;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $jumlahKategori = Kategori::count();
        $jumlahArtikel = Artikel::count();
        $jumlahUsers = User::count();
        return view('home', compact('jumlahKategori', 'jumlahArtikel', 'jumlahUsers'));
    }
}
